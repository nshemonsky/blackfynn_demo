variable "ami" {
  default = {
    "us-east-1" = "ami-97785bed"
  }
}

variable "key_path" {
  description = "Path to ssh key for access to system"
  default     = ""
}
