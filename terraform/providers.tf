provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "/Users/nickshemonsky/.aws/credentials"
  profile                 = "default"
}
